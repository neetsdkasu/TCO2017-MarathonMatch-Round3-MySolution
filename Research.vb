Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Research

    Public Sub Main()
        Dim numBottles As Integer = CInt(Console.ReadLine())
        Dim testStrips As Integer = CInt(Console.ReadLine())
        Dim testRounds As Integer = CInt(Console.ReadLine())
        Dim numPoison  As Integer = CInt(Console.ReadLine())

        Dim seed As Integer = CInt(Environment.GetCommandLineArgs()(1))
        Console.Error.WriteLine("seed: {0:D8}, nb: {1:D5}, ts: {2:D2}, tr: {3:D2}, np: {4:D3}", seed, numBottles, testStrips, testRounds, numPoison)
        
        Dim ret(numBottles - 1) As Integer
        For i As Integer = 0 To UBound(ret)
            ret(i) = i
        Next i

        Console.WriteLine(ret.Length)
        For Each r As Integer In ret
            Console.WriteLine(r)
        Next r
        Console.Out.Flush()
    End Sub

End Module