Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

    Public Sub Main()
        Dim numBottles As Integer = CInt(Console.ReadLine())
        Dim testStrips As Integer = CInt(Console.ReadLine())
        Dim testRounds As Integer = CInt(Console.ReadLine())
        Dim numPoison  As Integer = CInt(Console.ReadLine())

        Dim pw As New PoisonedWine()
        Dim ret() As Integer = pw.testWine(numBottles, testStrips, testRounds, numPoison)

        Console.WriteLine(ret.Length)
        For Each r As Integer In ret
            Console.WriteLine(r)
        Next r
        Console.Out.Flush()
    End Sub

End Module