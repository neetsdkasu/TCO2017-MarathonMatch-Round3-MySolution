Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports System
Imports System.Collections.Generic

Public Class PoisonedWine
    Dim flag(-1) As Integer
    Dim rand As New Random(1983)
    Const PC As Integer = 11  ' PC/TC = 11/10  Best Score with 1000 samples
    Const TC As Integer = 10  '(12/11 same score)
    
    Private Sub shuffle(ls As List(Of Integer))
        For i As Integer = 0 To ls.Count - 2
            Dim p As Integer = rand.Next(i + 1, ls.Count)
            Dim t As Integer = ls(i)
            ls(i) = ls(p)
            ls(p) = t
        Next i
    End Sub
    
    Private Sub smallCase(numBottles As Integer, testStrips As Integer, testRounds As Integer, numPoison As Integer)
        Dim unknowns As New List(Of Integer)(numBottles)
        Dim groups As New List(Of List(Of Integer))(testStrips + 1)
        Dim tests As New List(Of String)(testStrips)
        For t As Integer = 1 To testRounds
            unknowns.Clear()
            groups.Clear()
            tests.Clear()
            For i As Integer = 0 To UBound(flag)
                If flag(i) = 0 Then unknowns.Add(i)
            Next i
            If unknowns.Count = 0 Then
                If numPoison * PC <= TC * testStrips Then
                    For i As Integer = 0 To UBound(flag)
                        If flag(i) >= 0 Then unknowns.Add(i)
                    Next i
                Else
                    largeCase(numBottles, testStrips, testRounds - t + 1, numPoison)
                    Exit Sub
                End If
            End If
            shuffle(unknowns)
            Dim dv As Integer = (unknowns.Count + testStrips) \ (testStrips + 1)
            If dv = 0 Then dv = 1
            For i As Integer = 0 To testStrips
                Dim tmp As New List(Of Integer)(dv)
                For j = 0 To dv - 1
                    Dim k As Integer = i * dv + j
                    If k >= unknowns.Count Then Exit For
                    tmp.Add(unknowns(k))
                Next j
                If tmp.Count = 0 Then Exit For
                groups.Add(tmp)
            Next i
            For i As Integer = 0 To groups.Count - 2
                Dim test As String = ""
                For Each v As Integer In groups(i)
                    test += "," + CStr(v)
                Next v
                tests.Add(test.Substring(1))
            Next i
            Dim res() As Integer = PoisonTest.useTestStrips(tests.ToArray())
            Dim c As Integer = 0
            For i As Integer = 0 To UBound(res)
                If res(i) = 1 Then
                    testStrips -= 1
                    c += 1
                Else
                    For Each x As Integer In groups(i)
                        flag(x) = -999
                    Next x
                End If
            Next i
            If c = numPoison Then
                For Each x As Integer In groups(groups.Count - 1)
                    flag(x) = -999
                Next x
            End IF
            If testStrips < 1 Then Exit Sub
        Next t
    End Sub

    Private Sub largeCase(numBottles As Integer, testStrips As Integer, testRounds As Integer, numPoison As Integer)
        Dim numPoisonFirst As Integer = numPoison 
        Dim unknowns As New List(Of Integer)(numBottles)
        Dim groups As New List(Of List(Of Integer))(testStrips)
        Dim tests As New List(Of String)(testStrips)
        For t As Integer = 0 To testRounds - 1
            unknowns.Clear()
            groups.Clear()
            tests.Clear()
            If numPoison * PC <= TC * testStrips Then
                smallCase(numBottles, testStrips, testRounds - t, numPoison)
                Exit Sub
            End If
            For i As Integer = 0 To UBound(flag)
                If flag(i) = 0 Then unknowns.Add(i)
            Next i
            If unknowns.Count = 0 Then
                For i As Integer = 0 To UBound(flag)
                    If flag(i) >= 0 Then unknowns.Add(i)
                Next i
            End If
            shuffle(unknowns)
            Dim selU As Integer = 1
            Dim maxN As Integer = 0
            Dim minR As Double = 0.0
            For u As Integer = unknowns.Count \ numPoison To 1 Step -1
                Dim yx As Double = 1.0
                For i As Integer = 0 To numPoison - 1
                    yx *= CDbl(unknowns.Count - u - i)
                    yx /= CDbl(unknowns.Count - i)
                Next i
                Dim r As Double = 1.0 - yx
                Dim d1 As Integer = CInt(Math.Floor(CDbl(testStrips) * R))
                Dim d2 As Integer = testStrips - CInt(Math.Floor(CDbl(testStrips) * yx))
                Dim d As Integer = Math.Max(d1, d2)
                Dim n As Integer = u * (testStrips - d)
                If (n > maxN) OrElse (n = maxN And r < minR) Then
                    selU = u
                    maxN = n
                    minR = r
                End If
            Next u
            For i As Integer = 0 To testStrips - 1
                Dim tmp As New List(Of Integer)()
                Dim test As String = ""
                For j As Integer = 0 To selU - 1
                    Dim k As Integer = i * selU + j
                    If k >= unknowns.Count Then Exit For
                    test += "," + CStr(unknowns(k))
                    tmp.Add(unknowns(k))
                Next j
                If tmp.Count > 0 Then
                    tests.Add(test.Substring(1))
                    groups.Add(tmp)
                End If
            Next i
            Dim res() As Integer = PoisonTest.useTestStrips(tests.ToArray())
            For i As Integer = 0 To UBound(res)
                If res(i) = 1 Then
                    testStrips -= 1
                    numPoison -= 1
                    For Each x As Integer In groups(i)
                        flag(x) = 1
                    Next x
                Else
                    For Each x As Integer In groups(i)
                        flag(x) = -999
                    Next x
                End If
            Next i
            If numPoison = 0 Then
                For i As Integer = 0 To UBound(flag)
                    If flag(i) = 0 Then
                        flag(i) = -999
                    ElseIf flag(i) > 0 Then
                        flag(i) = 0
                    End If
                Next i
                numPoison = numPoisonFirst
            End If
            If testStrips < 1 Then Exit Sub
        Next t
    End Sub

    Public Function testWine(numBottles As Integer, testStrips As Integer, testRounds As Integer, numPoison As Integer) As Integer()
        ReDim flag(numBottles - 1)

        If numPoison * PC <= TC * testStrips Then
            smallCase(numBottles, testStrips, testRounds, numPoison)
        Else
            largeCase(numBottles, testStrips, testRounds, numPoison)
        End If

        Dim ret As New List(Of Integer)()
        For i As Integer = 0 To UBound(flag)
            If flag(i) >= 0 Then ret.Add(i)
        Next i
        testWine = ret.ToArray()
    End Function
End Class
