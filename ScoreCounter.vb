Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports System

Module ScoreCounter
    Sub Main()
        Dim line As String
        Dim total As Double = 0.0
        Dim count As Integer = 0
        Do
            line = Console.ReadLine()
            If line Is Nothing Then Exit Do
            Dim tokens() As String = line.Split(" "C)
            If tokens.Length = 0 OrElse tokens(0) <> "Score" Then
                Console.Error.WriteLine(line)
                Continue Do
            End If
            total += CDbl(tokens(2))
            count += 1
        Loop
        Dim average As Double = total / CDbl(count)
        Console.WriteLine("count {0}", count)
        Console.WriteLine("total {0}", total)
        Console.WriteLine("average {0}", average)
    End Sub
End Module