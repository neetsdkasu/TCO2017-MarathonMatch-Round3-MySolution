Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module PoisonTest
    Public Function useTestStrips(tests() As String) As Integer()
        Console.WriteLine("?")
        Console.WriteLine(tests.Length)
        For Each t As String In tests
            Console.WriteLine(t)
        Next t
        Console.Out.Flush()

        Dim n As Integer = CInt(Console.ReadLine())
        Dim res(n - 1) As Integer
        For i As Integer = 0 To n - 1
            res(i) = CInt(Console.ReadLine())
        Next i
        useTestStrips = res
    End Function
End Module